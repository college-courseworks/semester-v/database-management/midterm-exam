CREATE SCHEMA salam;

SET search_path TO salam;

CREATE TABLE salam.mahasiswas (
	nim CHAR(10) PRIMARY KEY,
	nama VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE,
	jenis_kelamin CHAR(1),
	CONSTRAINT cek_jenis_kelamin CHECK (jenis_kelamin IN ('L', 'P'))
);

CREATE OR REPLACE VIEW salam.mahasiswas_table
AS SELECT * FROM salam.mahasiswas;
