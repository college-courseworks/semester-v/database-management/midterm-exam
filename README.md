# Database Management - Midterm Exam

1217050103 - Muhammad Syamil Hamami

MBD Teori C - Praktikum E

Link PDF - [Klik Disini]()

> NOTE: File **.env** dalam praktik aslinya tidak disimpan di repository.

# Table of Contents

- [Menyiapkan Environment](#menyiapkan-environment)
- [Membuat Script Docker Compose](#membuat-script-docker-compose)
   - [Konfigurasi Docker Compose](#konfigurasi-docker-compose)
   - [Docker Compose Up](#docker-compose-up)
   - [Status Docker Setelah Compose Up](#status-docker-setelah-compose-up)
- [Cek Konektivitas dari pgAdmin dan DBeaver](#cek-konektivitas-dari-pgadmin-dan-dbeaver)
   - [pgAdmin](#pgadmin)
   - [DBeaver](#dbeaver)
- [Membuat Schema, Tabel dan Insert Data pada PostgreSQL](#membuat-schema-table-dan-insert-data-pada-postgresql)
   - [Membuat Schema dan Table (DDL - Data Definition Language)](#membuat-schema-dan-table-ddl---data-definition-language)
   - [Test Constraint](#test-constraint)
- [Membuat User pada Postgre](#membuat-user-pada-postgre)
   - [SQL Script untuk Membuat User dan Mengatur Akses](#sql-script-untuk-membuat-user-dan-mengatur-akses)
   - [Test Akses User](#test-akses-user)
- [Highlight](#highlight)

## Menyiapkan Environment

Siapkan environment variable dengan membuat file `.env` yang berisi seperti ini:

```yml
# Postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=ifunggul

# PGAdmin
PGADMIN_DEFAULT_EMAIL=pgadmin4@pgadmin.org
PGADMIN_DEFAULT_PASSWORD=ifunggul
PGADMIN_CONFIG_SERVER_MODE=False
```

## Membuat Script Docker Compose

### Konfigurasi Docker Compose

Setelah membuat file `.env` dan mengisi variabel yang akan digunakan, selanjutnya kita buat file `docker-compose.yml` dan jangan lupa buat satu direktori dengan file `.env`-nya. Berikut adalah isi dari file `docker-compose.yml`-nya:

```yml
version: "3.8"

services:
  postgres:
    image: postgres:latest
    container_name: postgres_container_msyamil
    environment:
      POSTGRES_USER: ${POSTGRES_USER} 
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
    volumes:
      - postgres_volumes:/data/postgres
    ports:
      - 21103:5432
    networks:
      - postgres_net
    restart: unless-stopped

  pgadmin:
    image: dpage/pgadmin4:latest
    container_name: pgadmin_container_msyamil
    environment:
      - PGADMIN_DEFAULT_EMAIL=${PGADMIN_DEFAULT_EMAIL}
      - PGADMIN_DEFAULT_PASSWORD=${PGADMIN_DEFAULT_PASSWORD}
      - PGADMIN_CONFIG_SERVER_MODE=${PGADMIN_CONFIG_SERVER_MODE}
    volumes:
      - pgadmin_volumes:/var/lib/pgadmin
    ports:
      - 22103:80
    networks:
      - postgres_net
    restart: unless-stopped

networks:
  postgres_net:
    driver: bridge

volumes:
  postgres_volumes:
  pgadmin_volumes:
```

Variabel yang terdapat pada `.env` digunakan dibagian **services - environment** dalam `docker-compose.yml`.

### Docker Compose Up

Output dari perintah `docker compose up -d`:

<div align="center">

![01](/screenshot/01.png)
</div>

### Status Docker Setelah Compose Up

Setelah berhasil melakukan perintah `docker compose up -d` maka akan muncul seperti ini jika menjalankan `docker ps` atau membuka GUI Docker jika menggunakan Docker Desktop:

<div align="center">

![02](/screenshot/02.png)
</div>

## Cek Konektivitas dari pgAdmin dan DBeaver

### pgAdmin

**pgAdmin** dapat dibuka di browser dengan alamat `localhost:22{tiga_nim_terakhir}` dalam kasus ini alamatnya adalah `localhost:22103`. 

<div align="center">

![03](/screenshot/03.png)
</div>

### DBeaver

Sama seperti mengkoneksikan lewat pgAdmin, hanya berbeda di port saja, karena DBeaver berada diluar jaringan dari Postgre-nya maka kita gunakan port yang diekspos yaitu `localhost:21{tiga_nim_terakhir}` dalam kasus ini adalah `localhost:21103`.

<div align="center">

![04](/screenshot/04.png)
</div>

Jika berhasil terkoneksi akan muncul tanda ceklis seperti gambar di atas.

## Membuat Schema, Table dan Insert Data pada PostgreSQL

### Membuat Schema dan Table (DDL - Data Definition Language)

Kita bisa buat schema dan table dengan GUI  melalui **DBeaver** atau menggunakan **SQL Script**, dalam kasus ini kita menggunakan **SQL Script**.

```sql
CREATE SCHEMA salam;

SET search_path TO salam;

CREATE TABLE salam.mahasiswas (
	nim CHAR(10) PRIMARY KEY,
	nama VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE,
	jenis_kelamin CHAR(1),
	CONSTRAINT cek_jenis_kelamin CHECK (jenis_kelamin IN ('L', 'P'))
);
```

### Test Constraint

#### Valid Insert

Berikut ini jika kita input data tanpa melanggar **Constraint**:

```sql
-- Valid Input
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050103', 'Muhammad Syamil Hamami', 'msyamil.hamami@gmail.com', 'L');
```

Maka data akan tersimpan di database dengan sukses.

<div align="center">

![05](/screenshot/05.png)
</div>

#### Primary Key Constraint

Lalu kita coba **INSERT** data dengan melanggar **Constraint Primary Key**, aturan dari **Primary Key** adalah tidak boleh kosong atau duplikat.

```sql
-- Violate Primary Key Constraint
INSERT INTO salam.mahasiswas (nama, email, jenis_kelamin)
VALUES ('Udin Sarkudin', 'udin.sarkudin@gmail.com', 'L');
```

Maka akan muncul pesan seperti ini:

<div align="center">

![06](/screenshot/06.png)
</div>

#### Unique Key Constraint

Selanjutnya kita tes **Unique Key Constraint**-nya.

```sql
-- Violate Unique Key Constraint
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050150','Udin Sarkudin', 'msyamil.hamami@gmail.com', 'L');
```

Hasilnya seperti ini:

<div align="center">

![07](/screenshot/07.png)
</div>

#### Check Constraint

Terakhir kita tes **Check Constraint**-nya, dalam aturan yang dibuat di **DML** sebelumnya, atribut `jenis_kelamin` hanya bisa diisi oleh char `L` atau `P` selain itu maka akan ditolak.

```sql
-- Violate Check Constraint
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050150','Udin Sarkudin', 'udin.sarkudin@gmail.com', 'Z');
```

Script di atas menginput `Z` pada atribut `jenis_kelamin` yang mana melanggar aturan **Check** yang dibuat, maka hasilnya seperti ini:

<div align="center">

![08](/screenshot/08.png)
</div>



## Membuat User pada Postgre

### SQL Script untuk Membuat User dan Mengatur Akses

Berikut adalah Script SQL untuk membuat dan memberi akses:

```sql
CREATE USER backend_dev LOGIN PASSWORD 'backend';
CREATE USER bi_dev LOGIN PASSWORD 'bi';

GRANT USAGE ON SCHEMA salam TO backend_dev, bi_dev;

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA salam TO backend_dev;
GRANT SELECT ON ALL TABLES IN SCHEMA salam TO bi_dev;
```

Di script di atas kita buat dua user yaitu `backend_dev` (akses **CRUD** semua table) dan `bi_dev` (akses **SELECT** semua table/view)

### Test Akses User

> NOTE: 
> - **<postgres 2>Console** ini console yang dibuka melalui koneksi `backend_dev`
> - **<postgres 3>Console** ini console yang dibuka melalui koneksi `bi_dev`

#### backend_dev

Kita coba test untuk melakukan **INSERT** data yang seharusnya `backend_dev` bisa melaukuan perintah ini.

<div align="center">

![09](/screenshot/09.png)
</div>

Dan yah, `backend_dev` bisa **INSERT** data, yang mana perintah **CRUD** lain juga bisa dilakukan

#### bi_dev

Selanjutnya `bi_dev`, kita coba melakukan **INSERT** data yang harusnya tidak bisa karena ia hanya diberi akses **SELECT** saja.

<div align="center">

![10](/screenshot/10.png)
</div>

Bisa dilihat di gambar atas, `bi_dev` tidak bisa melakukan **INSERT**, selanjutnya kita coba melakukan perintah **SELECT** pada table/view.

<div align="center">

![11](/screenshot/11.png)
</div>

Voilà.., user `bi_dev` bisa melakukannya, dengan melakukan test ini bisa kita ketahui kalau konfigurasi user telah berhasil.

## Highlight
1. Pekerjaan di atas hanya menggunakan Docker CLI saja, tanpa menginstall Docker Desktop.
2. Sebelumnya berniat menggunakan versi Postgre yang menggunakan Alpine Linux, tetapi di ketentuan harus menggunakan tag Latest, jadi takutnya melanggar ketentuan.
3. Membuat view dalam Postgresql.

   ```sql
   CREATE OR REPLACE VIEW salam.mahasiswas_table
   AS SELECT * FROM salam.mahasiswas;
   ```
4. Dokumentasi lengkap dan detail, dan juga ada dua format yaitu dokumen pdf dan markdown di Gitlab.
5. Terakhir, semua pekerjaan di atas dilakukan full dalam sistem operasi Linux, dari mulai instalasi Docker, konfigurasi Post-Installation Docker, sebagian script juga ditulis dengan text editor Neovim, sampai instalasi DBeaver.

<div align="center">

![12](/screenshot/12.png)
![13](/screenshot/13.jpeg)
</div>