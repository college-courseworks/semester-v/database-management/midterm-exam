-- Valid Input
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050103', 'Muhammad Syamil Hamami', 'msyamil.hamami@gmail.com', 'L');

-- Violate Primary Key Constraint
INSERT INTO salam.mahasiswas (nama, email, jenis_kelamin)
VALUES ('Udin Sarkudin', 'udin.sarkudin@gmail.com', 'L');

-- Violate Unique Key Constraint
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050150','Udin Sarkudin', 'msyamil.hamami@gmail.com', 'L');

-- Violate Check Constraint
INSERT INTO salam.mahasiswas (nim, nama, email, jenis_kelamin)
VALUES ('1217050150','Udin Sarkudin', 'udin.sarkudin@gmail.com', 'Z');
